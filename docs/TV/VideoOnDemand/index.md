# Video On Demand
Considerando los requerimientos que ministra solicita para publicar un video bajo demanda, puede solucionarse la disponibilidad del contenido dentro de la red, cargando los archivos en un servidor web.

Para ello, el laboratorio de catv cuenta con un contenedor que tiene instalado el servicio **Apache**, el mismo cuenta con la carpeta **/var/www/html/** relacionada con el directorio **/home/catv/html/** de la pc anfitrión, donde puede cargarse directamente el contenido.

El acceso al contenido es de la forma:

```bash
http://"IPservidor"/"Nombre"
```

Donde:  ||
-- | --|
**IPservidor** = Dirección IP del contenedor que contiene Apache. | **Nombre** = Nombre asignado al archivo que se encuentra en el servidor.|