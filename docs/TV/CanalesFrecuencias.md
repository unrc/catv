# Canales y frecuencias para TDA Argentina (región de Río Cuarto)

Canal | Nombre | Frecuencias [Mhz] |
:-: | :-: | :-: |
22 | Encuentro - Paka Paka - Mirador - CineAR - TecTV | 521 |
23 | Televisión Pública - ConstruirTV | 527 |
24 | Deportv HD - Canal 26 - France 24 - Cronica TV - IP | 533 |
25 | CN23 - C5N - LN+ - 360 - RT | 539 |
29 | Imperio televisión (Canal 13) | 563 |
31 | UnirioTV | 575 |