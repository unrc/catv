#Mapeo de canales

## Canales resultantes
Para conocer el direccionamiento de cada uno de los canales, se puede acceder a la unl: 

    http://"IPserver":"Puerto"/channels_list.html

## Canales Unicast
Por otro lado, se puede descargar una lista de reproducción a partir de la cual se puede reproducir (por ejemplo con vlc), los canales de tv desde la url:

    http://"IPserver":"Puerto"/playlist.m3u

Donde: ||
--|-- |
**IPserver** = Dirección IP del servidor de televisión | **Puerto** = Puerto indicado en la configuración unicast |