# MuMuDVB

MuMuDVB (Multi Multicast DVB) es un programa gratuito para streaming de TV basado en dvbstream. 

Puede distribuir un stream desde una fuente DVB, como puede ser una señal de televisión satelital, terrestre, de cable digital o ATSC.

La principal característica es dividir el contenido de un transporter en múltiples canales y transmitir cada uno de ellos en diferentes grupo multicast o en unicast utilizando HTTP.

Sitio oficial: [mumudvb.net](https://mumudvb.net/)

## Instalación

La instalación de mumudvb puede realizarse directamente desde el terminal con el comando apt. El instalador se encuentra en el repositorio de linux por defecto.

```bash
sudo apt update
sudo apt install mumudvb
```
### Descarga del instalador

En caso de querer descargar el instalador, ya que puede ser necesario para otras versiones de Linux, el sitio oficial es [link](https://mumudvb.net/download/)

El mismo puede instalarse también en Raspberry pi.