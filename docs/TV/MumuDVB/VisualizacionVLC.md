# Visualización utilizando VLC

## Contenido Multicast

Acceder a la opción de stream por red:

Media > Open Network Stream ...

![](imagenes/ReproduccionVLC1.jpg)

Luego ingresar la url del contenido:

![](imagenes/ReproduccionVLC.jpg)

La dirección debe tener la siguiente estructura: "udp://@**IPmulticast**:**Puerto**"

Donde: ||
:-: | :-: |
**IPmulticast** = Dirección IP multicast del contenido | **Puerto** = Puerto del contenido |

## Contenido Unicast
