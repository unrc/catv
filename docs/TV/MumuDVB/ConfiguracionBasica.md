# Configuración Básica

Para más opciones de configuración: [enlace](https://mumudvb.net/documentation/asciidoc/mumudvb-2.1.0/README_CONF.html)

## Archivo de configuración

El funcionamiento de mumudvb, puede configurarse por medio de un archivo con extensión *.conf.

### Sintonía de canales

Inicialmente, se debe indicar los aspectos que hacen al sdr que se ha de utilizar y los parámetros del canal analógico desde el que se quiere acceder al contenido:

```bash
card="#Tarjeta"
freq="Frecuencia"
bandwidth="BWcanal"
autoconfiguration="modo"
```

Donde: | |
-- | -- |
**#Tarjeta** = número de sdr | **Frecuencia** = frecuencia del canal |
**BWcanal** = Ancho de banda del canal | **modo** = indica el modo que se va a recopiar la información dentro del canal analógico (utilizar **full**, ya que de esta forma se configura en forma automática) |

### Multicast

Una vez sintonizado el canal analógico, se puede transmitir las distintas señales de televisión en diferentes direcciones de IP multicast. Para ello se puede ingresar los primeros 3 octetos de una dirección ip de multicast, seguido por **%number**, de forma tal que el último octeto se asigne en forma automática. Esto puede verse en el siguiente ejemplo:

autoconf_ip4=239.100.23.%number

### Unicast

En caso de querer que el servidor funcione en unicast, se deben ingresar como mínimo los siguientes parámetros:

unicast=1

port_http="Puerto"

store_eit=1

Donde: | |
-- | -- |
**Puerto** = puerto en capa 4 |  |

## Ejecución

```bash
mumudvb -d -c "ArchivoDeConfiguracion.conf" 
```

Donde: |
--|
**ArchivoDeConfiguracion.conf** = es el nombre asociado a la configuración del servidor, con extensión *.conf |

##Webservices

https://mumudvb.net/documentation/asciidoc/mumudvb-2.0.0/WEBSERVICES.html