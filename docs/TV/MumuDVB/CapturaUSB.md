
https://www.youtube.com/watch?v=bhZqA1By314&ab_channel=BrendenAdamczak

Para habiliar la captura en el puerto usb, es necesario ejecutar el siguiente comando:

```bash
sudo modprobe usbmon
```

Por medio del siguiente comando, es posible conocer el número de **bus** y a partir del mismo, seleccionar dónde realizar la captura con wireshark:

```bash
lsusb
```