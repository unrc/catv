# Implementación

## Diagrama de red disponible

![](./TV-Infraestructura.png)

<div align='center'>Figura 1. Diagrama de red de elementos relacionados a red de tv</div>

## Configuración de servicios de IPtv/OTT/VoD

En esta sección se explican los pasos para brindar los servicios de TV y Video on Demand.
Para ello, inicialmente se deben configurar el servidor de streaming **MumuDvB**, como fuente del contenido multicast (IPtv) o unicast (OTT). En paralelo, se debe montar un servidor donde se pueda alojar el contenido para Video on Demand:

### MumuDvB
A rasgos generales, los pasos para la implementación del servidor MumuDvB son:

![](./MumuDVB/imagenes/MumuDvB-Implementacion.png)

<div align='center'>Figura 2. Principales pasos de configuración de MumuDvB</div>

### Servidor VoD

A rasgos generales, los pasos para la implementación del servidor Ministra y clientes en SetTopBox son:

![](./VideoOnDemand/imagenes/VoD-Implementacion.png)

<div align='center'>Figura 2. Pasos para la carga de contenido VoD</div>

### Ministra

A rasgos generales, los pasos para la implementación del servidor Ministra y clientes en SetTopBox son:

![](./Ministra/imagenes/Ministra-Implementacion.png)

<div align='center'>Figura 2. Principales pasos de configuración de Ministra</div>