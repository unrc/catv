#Carga Clientes

En la sección (1) **Users** > (2) **Users list**, se debe seleccionar la opción (3) **+ Add userl**, como se muestra en la siguiente imagen:

![](1GestionClientes.png)

<div align='center'>Figura 1. Seccion clientes.</div>

Una vez dentro de la carga de un nuevo cliente, es necesario ingresar todos los datos del abonado (4) y luego guardar los cambios (5).

![](2GestionClientes.png)

<div align='center'>Figura 2. Carga de cliente.</div>

Una vez cargado el abonado, se puede habilitar/deshabilitar (6) o acceder a otras configuraciones (7).

![](3GestionClientes.png)

<div align='center'>Figura 3. Cambios en cliente.</div>