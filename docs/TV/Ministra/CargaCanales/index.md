# Carga de Canales

## Carga manual

En la sección (1) **IPTV Channels** > (2) > **Channels**, se debe seleccionar la opción (3) **+ add channel**, como se muestra en la siguiente imagen:

![](1CargaDeCanal.png)

<div align='center'>Figura 1. Inicio de carga de un canal.</div>

Dentro de la oción **+ add channel**, se debe realizar la configuración básica, siguiendo los pasos que se observan en la figura 2:

![](2CargaDeCanal.png)

<div align='center'>Figura 2. Carga de parámetros iniciales.</div>

4- Ingresar el número dentro de la grilla que se le asigna alnuevo canal.

5- Indicar un nombre, para identificar el nuevo canal.

6- Indicar el género asociado al contenido del canal.

7- Agregar a grilla básica. **IMPORTANTE:** si no se genera una grilla básica o una grilla en particular, los usuarios no tendrán canales asignados, por lo que se recomienda que para hacer pruebas, se cargue al menos un canal con esta opción habilitada. 

8- Agregar el enlace asociado al stream del canal. La nueva ventana que se abre como resultado de clickear esta opción se muestra en la figura 3:

![](3CargaDeCanal.png)

<div align='center'>Figura 3. Carga de URL.</div>

9- Ingresar la URL asociada al canal.

10- Guardar el cambio.

En la Figura 4 se presentan otras opciones adicionales que pueden ingresarse al nuevo canal. En caso de no ser necesarias, sólo queda guardar los cambios, como se indica en el paso 10:

![](4CargaDeCanal.png)

<div align='center'>Figura 4. Guardar cambios.</div>

## Carga a partir de playlist

Para no tener que cargar uno a uno los canales en forma manual, se puede utilizar las playlist que se obtienen en MumuDvB, como se muestra en el [enlace](https://unrc.gitlab.io/catv/MumuDVB/MapeoCanales/). Una vez se descaga la lista de reproducción en la PC, se deben seguir los siguientes pasos:

En la sección (1) **IPTV Channels** > (2) **Import from m3u**, se debe seleccionar la opción (3) **Add a file**, como se muestra en la siguiente imagen:

![](1import_playlist.png)

<div align='center'>Figura 5. Importar canales desde playlist.</div>

Una vez importada la lista de reproducción, se pueden modificar los parámetros de cada canal y se puede guardar o descartar cada uno, en función a lo indicado en el punto 6:

![](2import_playlist.png)

<div align='center'>Figura 6. Configuración de canales importados.</div>