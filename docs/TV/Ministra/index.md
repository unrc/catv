# Introducción

Ministra es una plataforma de TV multipantalla para la gestión de proyectos IPTV/OTT. Este desarrollo de código abierto se compone de módulos, lo que permite introducir nuevas funcionalidades. La plataforma de TV brinda compatibilidad con los dispositivos más solicitados: decodificadores de TV, Smart TV, dispositivos móviles y computadoras personales.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Xhx8ns-Gcy4?si=t4dQn7Dm_f-QOJcT" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Sitio web oficial: [ttps://www.infomir.eu/esp/](https://www.infomir.eu/esp/)