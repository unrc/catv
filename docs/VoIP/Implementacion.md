# Implementación

## Infraestructura

![](./imagenes/Asterisk-Infraestructura.png)

<div align='center'>Figura 1. Principales elementos de red de VoIP</div>

## Configuración Básica

A rasgos generales, los pasos para la implementación del servidor Asterisk son:

![](./imagenes/Asterisk-Implementacion.png)

<div align='center'>Figura 2. Pasos para la configuración básica de Asterisk y Teléfonos</div>