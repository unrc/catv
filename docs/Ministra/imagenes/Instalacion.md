# Instalación

Para la utilización de este servicio se requiere un contenedor que implemente Ministra y otro que gestione y almacene una base de datos MySQL.

## Creación de bridge

Crear una red  privada en donde ministra y MySQL se puedan comunicar.

```bash
$ docker network create --driver bridge ministra-net
```

## Instalación de base de datos en MySQL

### Creación de un volumen para MySQL

Crear un volumen para los archivos de la base de datos de MySQL se almacenen en la PC anfitrión:

```bash
$ docker volume create mysql-server
```

### Carga del contenedor MySQL

```bash
$ docker run \
--name mysql-server \
-v mysql-server:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=some_password \
--network ministra-net -p 127.0.0.1:3306:3306 \
-h mysql-server \
-d mysql:5.7 --sql-mode="NO_ENGINE_SUBSTITUTION"
```
## Instalación de Ministra

### Descarga de imagen 

```bash
$ docker pull scriptingonline/ministra
```

### Carga de archivo de configuración

En el directorio del usuario, crear un archivo con el nombre "custom.ini":

```bash
$ cd ~
$ nano custom.ini
```
Dentro del archivo, pegar el siguiente contenido:

```bash
[l18n]
default_timezone = Europe/London

[database]
mysql_host = mysql-server
mysql_port = 3306
mysql_user = root
mysql_pass = some_password
db_name = stalker_db
```

### Inicialización del contenedor "Ministra"

Para iniciar el contenedor, ejecutar el siguiente comando:

```bash
docker run -it \
-e MYSQLPASS='some_password' \
-e MYSQLADDR='mysql-server' \
--name ministra -p 127.0.0.1:80:80 \
--mount type=bind,source=$(pwd)/custom.ini,target=/var/www/html/stalker_portal/server/custom.ini \
--network ministra-net scriptingonline/ministra:latest
```

### Acceso al entorno web

Para acceder al entorno web, la url es:[http://localhost/stalker_portal/server/adm/](http://localhost/stalker_portal/server/adm/)
Tener presente que en caso que el contenedor no se encuentre en la PC local, se debe cambiar **localhost** por la dirección IP asociada al dispositivo donde se aloje Ministra.

![](./imagenes/login.png)

Una vez cargado el sitio, acceder con los siguientes parámetros:

Nombre de usuario: admin 

Contraseña: 1