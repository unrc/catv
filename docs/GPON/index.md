# Introducción

Elementos disponibles dentro de la marca ubiquiti: [https://dl.ubnt.com/ds/uf_gpon.pdf](https://dl.ubnt.com/ds/uf_gpon.pdf)

## OLT

Modelo: Ubiquity UF-OLT-4

Guia de instalación / especificaiones: [https://dl.ubnt.com/qsg/UF-OLT-4/UF-OLT-4_ES.html](https://dl.ubnt.com/qsg/UF-OLT-4/UF-OLT-4_ES.html)

## ONU

Modelo: UF-WIFI

Guia de instalación / especificaiones: [https://dl.ui.com/qsg/UF-WIFI/UF-WIFI_ES.html](https://dl.ui.com/qsg/UF-WIFI/UF-WIFI_ES.html)