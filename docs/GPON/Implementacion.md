# Implementación

## Diagrama de red disponible

![](./imagenes/GPON-Infraestructura.png)

<div align='center'> Figura 1. Esquema general de elementos presentes en el laboratorio de catv</div>

## Pasos básicos de implementación

A rasgos generales, los pasos para la implementación de GPON son:

![](./imagenes/GPON-Implementacion.png)

<div align='center'> Figura 2. Esquema de pasos principales de configuración</div>