#Acceso - Página inicial

## Acceso - Configuración por defecto

Una vez el equipo se encuentre con la configuración por defecto, es necesario acceder con un navegador web, a la siguiente url: [http://192.168.1.20](http://192.168.1.20). Tener presente que la PC desde la que se intente la conexión debe pertenecer a la red 192.168.1.0/24. Al cargarse, se obtendrá un entorno de acceso de la forma:

![](./imagenes/Acceso.png)

<div align='center'>Figura 1. Entorno de acceso</div>

## Página Principal

![](./imagenes/Principal.png)

<div align='center'>Figura 2. Entorno principal</div>

Dentro del entorno web, se puede identificar los siguientes puntos:

1 - Carga de tráfico (subida y bajada) de interfaz sfp+.

2 - Cantidad de ONUs, diferenciando aquellas que se encuentran activas del total que la OLT ha identificado.

3 - Señal del conjunto de ONUs.

4 - Numeración IPv4 y Dirección MAC de la OLT.

5 - Reprecentación de las insterfaces activas/inactivas.

6 - Descripción de cada una de las interfaces GPON.

7 - Descripición de la interfaz SFP+.