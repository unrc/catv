#Configuración de ONUs

## Listado de ONUs

En la segunda opción del menú de la izquierda, es posible conocer el listado de ONUs, ya sea aquellas que están conectadas como las que fueron desconectadas. A su vez, se puede conocer aspectos básicos de su funcionamiento:

![](imagenes/ONUs/ListadoONUs.png)

<div align='center'>Figura 1. Listado de ONUs</div>

En la imagen se analizan los siguientes aspectos:

1 - Selección de una o más onus

2 - Nombre o dirección MAC para identificar cada ONU.

3 - Modelo de ONU.

4 - Modo de configuración (Router/Bridge)

5 - Estado de los puertos de cada ONU.

6 - Tasa de transferencia de subida y bajada.

7 - Nivel de señal [dBm].

8 - Porcentaje de calidad de conexión.

9 - Tiempo desde que la ONU se conectó con la OLT.

10 - Configuración por defecto, que se aplica a todas las ONUs, salvo que se le implemente alguna configuración en particular.

## Configuración

En este tutorial se desarrollarán los aspectos básicos de configuración centralizada de ONUs a partir de la OLT. Para ello, se contempla cómo cargar una configuración por defecto, compartida por todos los clientes. Para ello, se debe seleccionar la opción "Default ONU configuration", presente en la parte superior derecha del listado de ONUs, presente en la figura 1.

Es posible configurar una ONU en particular, accediendo desde el ícono en forma de engranaje, presente en el lado derecho.

![](imagenes/ONUs/ONUparticular.png)

<div align='center'>Figura 2. Configuración de una ONU</div>

Por otra parte, se pueden ejecutar opciones avanzadas sobre cada onu en particular, seleccionando el ícono de 3 puntos, presente a la derecha:

![](imagenes/ONUs/extra.png)

<div align='center'>Figura 3. Opciones avanzadas sobre una ONU</div>

### Basic

Al ingresar a la opción de configuración de ONUs, se accede a la configuración básica, como se muestra en las figuras 4 y 5. En las mismas se observa:

1 - Habilitar/deshabilitar la conexión de la ONU a la red.

2 - Clave de administración.

3 - Selección de modo de funcionamiento, ver a continuación en próximas secciones.

4 - Habilitar/deshabilitar la gestión de la ONU desde la OLT.

5 - Habilitar/deshabilitar IPv4.

6 - Opción de limitar velocidad de subida y bajada.

#### Modo Bridge

En este caso, el dispositivo funciona como un conversor óptico<->eléctrico, por lo que no se implementa un ruteo entre la interfaz GPON y las interfaces eléctricas.

![](imagenes/ONUs/BasicBridge.png)

<div align='center'>Figura 4. Configuración Básica, modo bridge</div>

7 - Configuración de todos los parámetros asociados para el acceso al dispositivo en forma local.

8 - La configuración de parámetros de interfaz WAN (conexión entre la ONU y OLT) se encuentra deshabilitada, ya que no corresponde.

#### Modo Router

En este modo, se implementan dos redes diferentes, una asociada a la interfaz WAN (correspondiente a la red GPON) y las interfaces internas, pertenecientes al cliente, las cuales pueden enrutarse.

7 - Configuración de todos los parámetros asociados a la red interna del cliente.

8 - Configuración de parámetros de interfaz WAN (conexión entre la ONU y OLT).

9 - Tener presente que en este modo se habilita la función de **Firewall**.

![](imagenes/ONUs/BasicRouter.png)

<div align='center'>Figura 5. Configuración Básica, modo route</div>

### VLAN

En la sección **VLAN**, se puede realizar un mapeo de puertos ethernet y VLANs:


![](imagenes/ONUs/VLAN.png)

<div align='center'>Figura 6. Mapeo puertos ethernet con vlans</div>

### Wireless

Dentro de las opciones de configuración de wifi, se pueden identificar las siguientes:

![](imagenes/ONUs/Wireless.png)

<div align='center'>Figura 7. Configuración redes inalámbricas</div>

1 - Habilitar la gestión centralizada desde la OLT.

2 - Indicar el país - especifica la gestión de frecuencias.

3 - Habilitar red en 2.4 Ghz.

4 - Habilitar red en 5.8 Ghz.

5 - Configuración de redes inalámbricas, según corresponda:

* SSID = Nombre de red.

* Hide SSID = Ocultar nombre de red.

* Encryption = Tipo de encriptación de contraseña de red.

* Password = Contraseña de la red.

* Channel = Canal, dependiente de la región especificada en el punto 2.

* Channel width = Ancho de banda del canal.

* Output power = potencia de transmisión.



### Firewall

![](imagenes/ONUs/Firewall.png)

<div align='center'>Figura 8. Configuración Firewall</div>

### Ports

En el área **ports** se puede especificar la velocidad de cada uno de los puertos ethernet:

![](imagenes/ONUs/Ports.png)

<div align='center'>Figura 9. Configuración puertos ethernet</div>

### Services

Como se muestra en la siguiente figura, en la sección **services**, se puede habilitar/deshabilitar el acceso web o ssh, indicando a su vez los puertos por los cuales se puede conectar:


![](imagenes/ONUs/Services.png)

<div align='center'>Figura 10. Configuración Servicios</div>

