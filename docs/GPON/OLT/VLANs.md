# VLANs

En esta sección es posible la carga y edición de las vlans que utilizará la OLT desde y hacia la red GPON, dado que la misma funcionará como un troncal.

![](imagenes/VLANs.png)

Como se observa en la figura anterior, las opciones son:

1 - Visualización de número de vlan.

2 - Nombre asignado a la vlan.

3 - Modo de funcionamiento. Al presionar sobre ese recuadro se puede elegir entre:
        
**U**: Sin etiquetado
**T**: Con etiquetado
**E**: Exclusión, no se permite el flujo de la vlan en cuestión.

4 - Carga de una nueva vlan.