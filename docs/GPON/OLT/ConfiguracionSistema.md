# Configuración de Sistema

![](imagenes/ConfiguracionSistema.png)

1 - Nombre del equipo

2 - 

3 - Configuración de red para el acceso a la OLT. Tener presente que en la parte inferior se habilita/deshabilita la opción de gestión por medio de la interfaz SFP+ y vlan. Esto implica que se puede administrar el equipo en el mísmo vínculo por el que se conecta a la red de datos. Deshabilitar esta opción puede aumentar la seguridad, por lo que sólo sería accecible desde el puerto eléctrico de gestión. De quedar habilitado, se recomienda implementar una VLAN exclusiva para la gestión de los dispositivos de red.
 
4 - Sección para habilitar/deshabilitar servicios.

5 - Descarga de archivo con resguardo de configuración.

6 - Carga de archivo con configuración de resguardo.

7 - Carga de localización del equipo.

8 - Descarga de archivo de configuración.

9 - Actualización de Firmware.

10 - Resetear el equipo.

11 - Cargar configuración por defecto.